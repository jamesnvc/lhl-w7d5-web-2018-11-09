Rails.application.routes.draw do
  resources :books, only: [:index, :show]

  # This just sets up routes in such a way that we can access the same
  # book controller routes under authors as well
  # If we want to do something with the author that the current URL is
  # associated with, we need to do that ourselves (see code we added
  # to the bookscontroller)
  resources :authors do
    resources :books, only: [:index, :show]
  end

  # Says that another controller (in this case admin/bookscontroller)
  # should handle the given routes; sets up routes sort of like nested
  # resources, but *also* points the action to a different controller
  namespace :admin do
    resources :books, except: [:index, :show]
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
