class BooksController < ApplicationController
  before_action :set_book, only: [:show]
  before_action :set_author, only: [:index]

  # GET /books
  # GET /books.json
  def index
    if @author.present?
      @books = @author.books
    else
      @books = Book.all
    end
  end

  # GET /books/1
  # GET /books/1.json
  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_book
      @book = Book.find(params[:id])
    end

    def set_author
      # Doing find_by(id: ..) instead of just find(..) because when
      # going to the non-nested /books route, there is no author_id in
      # params & find(..) throws an exception if the id is nil
      @author = Author.find_by(id: params[:author_id])
    end
end
