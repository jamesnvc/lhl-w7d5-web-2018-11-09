json.extract! author, :id, :name, :primary_genre, :birthday, :created_at, :updated_at
json.url author_url(author, format: :json)
